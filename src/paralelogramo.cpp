#include "paralelogramo.hpp"


Paralelogramo::Paralelogramo(float ladoMaior, float ladoMenor, float altura){
    set_tipo("Paralelogramo");
    set_ladoMaior(ladoMaior);
    set_base(ladoMenor);
    set_altura(altura);
    cout << "Construtor da classe Paralelogramo"<< endl;
}

Paralelogramo::~Paralelogramo(){
    cout << "Destrutor da classe Paralelogramo" << endl;
}

float Paralelogramo::get_ladoMaior(){
    return ladoMaior;
}
void Paralelogramo::set_ladoMaior(float ladoMaior){
    if(ladoMaior < 0)
        throw(1);
    else
        this->ladoMaior = ladoMaior;
}

float Paralelogramo::calcula_area(){
    return get_altura()*get_base();
}

float Paralelogramo::calcula_perimetro(){
    return 2*(ladoMaior+get_base());
}


