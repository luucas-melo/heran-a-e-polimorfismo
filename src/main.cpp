#include "formageometrica.hpp"
#include "triangulo.hpp"

#include <vector>

int main(){
   
    FormaGeometrica forma;
    Triangulo triangulo;
    //Losango losango;
   // Paralelogramo paralelogramo;

    vector <FormaGeometrica *> listaFormas;

    listaFormas.push_back(new FormaGeometrica(3.0, 4.0));
    listaFormas.push_back(new Triangulo("Triângulo Retangulo", 3.0, 4.0));
    //listaFormas.push_back(new Losango(30.0, 40.0));
   // listaFormas.push_back(new Paralelogramo(40.0,10.0,5.0));


    for(FormaGeometrica * f: listaFormas){
        cout << "Tipo: " << f->get_tipo() << endl;
        cout << "Área: " << f->calcula_area() << endl;
        cout << "Perímetro: " << f->calcula_perimetro() << endl << endl;
    }

    return 0;
}