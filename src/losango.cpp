#include "losango.hpp"


Losango::Losango(float diagonalMaior, float diagonalMenor){
    set_tipo("Losango");
    set_base(diagonalMenor);
    set_altura(diagonalMaior);
    cout << "Construtor da classe Losango"<< endl;
}

Losango::~Losango(){
    cout << "Destrutor da classe Losango" << endl;
}



float Losango::calcula_area(){
    return get_altura()*get_base()/2;
}

float Losango::calcula_perimetro(){
    float lado = sqrt(pow(get_base()/2,2)+ pow(get_altura()/2,2));
    return 4*lado;
}


