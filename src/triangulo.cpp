#include "triangulo.hpp"

Triangulo::Triangulo(){
    set_tipo("Triangulo Retangulo");
    set_altura(4);
    set_base(3);
    cout << "Construtor da classe Triangulo"<< endl;
};
Triangulo::Triangulo(string tipo, float base,float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
    cout << "Construtor da classe Triangulo"<< endl;
}

Triangulo::~Triangulo(){
    cout << "Destrutor da classe Triangulo" << endl;
}



float Triangulo::calcula_area(){
    return (get_base()*get_altura())/2;
}

float Triangulo::calcula_perimetro(){
    float hipotenusa = sqrt(pow(get_base(),2)+pow(get_altura(),2));
    return get_base()+get_altura()+hipotenusa;
}


