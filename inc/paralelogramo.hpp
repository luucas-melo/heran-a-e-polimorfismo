#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"
#include <iostream>
#include <string>


using namespace std;

class Paralelogramo : public FormaGeometrica{
    private:
        float ladoMaior;
    public: 
        Paralelogramo();
        Paralelogramo(float ladoMenor, float ladoMaior, float altura);
        ~Paralelogramo();    
        float calcula_area();
        float calcula_perimetro();
        float get_ladoMaior();
        void set_ladoMaior(float ladoMaior);
};
#endif