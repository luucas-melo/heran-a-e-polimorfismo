#ifndef LOSANGO_HPP
#define LOSANGO_HPP
#include "formageometrica.hpp"
#include <iostream>
using namespace std;
class Losango : public FormaGeometrica{
    public:
        Losango();
        Losango(float diagonalMaior, float diagonalMenor);
        ~Losango();
        float calcula_area();
        float calcula_perimetro();
};




#endif