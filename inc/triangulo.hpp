#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include "formageometrica.hpp"
#include <iostream>
#include <string>


using namespace std;

class Triangulo : public FormaGeometrica{
    public: 
        Triangulo();
        Triangulo(string tipo, float base,float altura);
        ~Triangulo();    
        float calcula_area();
        float calcula_perimetro();

};
#endif